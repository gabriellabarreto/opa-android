package com.example.projetoprimeiroestagio

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.example.projetoprimeiroestagio.entities.Produto
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ListarActivity : AppCompatActivity() {

    lateinit var listView: ListView
    lateinit var addButton: FloatingActionButton
    lateinit var listProdutos: ArrayList<Produto>
    lateinit var arrayAdapterProduto: ArrayAdapter<Produto>

    companion object {
        const val REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listar)

        listView = findViewById(R.id.listView)
        addButton = findViewById(R.id.addButton)

        listProdutos = ArrayList<Produto>()
        listProdutos.add(Produto("Coxinha", "4.0", "5", "Frango"))

        arrayAdapterProduto =
            ArrayAdapter(ListarActivity@ this, android.R.layout.simple_list_item_1, listProdutos)

        listView.adapter = arrayAdapterProduto

        listView.setOnItemClickListener { parent, view, position, id ->
            var produto = listProdutos[position]
            var i = Intent(ListarActivity@ this, ExibirActivity::class.java)
            i.putExtra("produto", produto)
            i.putExtra("position", position)
            startActivityForResult(i, REQUEST_CODE)
            //startActivity(i)
        }

        addButton.setOnClickListener {
            var i = Intent(ListarActivity@ this, CadastrarActivity::class.java)
            startActivityForResult(i, REQUEST_CODE)
        }

        listView.setOnItemLongClickListener{ parent, view, position, id ->
            var produto = listProdutos[position]
            listProdutos.remove(produto)
            arrayAdapterProduto.notifyDataSetChanged()

            Toast.makeText(CadastrarActivity@this, "Produto excluido!", Toast.LENGTH_LONG).show()

            true

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == CadastrarActivity.RESULT_CADASTRAR) {
            var produto = data?.getParcelableExtra<Produto>("produto")
            if (produto != null) {
                listProdutos.add(produto)
                arrayAdapterProduto.notifyDataSetChanged()
            } else {
                Log.e("MainActivity", "Erro ao retornar uma produto")
            }
        }
        if(requestCode == REQUEST_CODE && resultCode == ExibirActivity.RESULT_ALTERAR){
            var produto = data?.getParcelableExtra<Produto>("produto")
            var indice = data?.getIntExtra("position", 0)
            if (produto != null && indice!= null) {
                listProdutos.set(indice, produto)
                arrayAdapterProduto.notifyDataSetChanged()
            }
        }

    }

}
